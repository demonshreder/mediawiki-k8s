FROM registry.access.redhat.com/ubi8/ubi-init
#FROM    redhat/ubi8-init
    MAINTAINER Kamalavelan <sskamalavelan@gmail.com>
RUN dnf -y update
RUN dnf module reset php
RUN dnf -y module enable php:7.4
RUN dnf -y install httpd php php-mysqlnd php-gd php-xml wget php-mbstring php-json php-intl php-apcu
RUN dnf -y install nano iproute less
RUN wget https://releases.wikimedia.org/mediawiki/1.38/mediawiki-1.38.1.tar.gz
RUN tar -zxf mediawiki-1.38.1.tar.gz
RUN mv mediawiki-1.38.1/* /var/www/
COPY httpd.conf /etc/httpd/conf.d/mediawiki.conf
RUN mkdir /run/php-fpm
RUN touch /run/php-fpm/www.sock
RUN php-fpm -D
RUN echo "ServerName localhost" | tee -a /etc/httpd/conf.d/httpd.conf
EXPOSE 80
COPY run.sh /var/www/
COPY LocalSettings.php /var/www/
RUN chown -R apache: /var/www
CMD ["/bin/bash","/var/www/run.sh"]
