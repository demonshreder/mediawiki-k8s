# Mediawiki-k8s

http://mwiki.cooponscitech.in

## Repo to host a demo for running mediawiki using Kubernetes

## Install

- kubectl apply -f app.yml

- kubectl apply -f db.yaml

- kubectl exec -it <mediawiki pod> /bin/bash

- cd /var/www/mediawiki/maintenance

- php install.php --dbname mediawiki --dbserver mwiki-db --dbuser root --dbpass mysql-root-password mediawiki dbadminuser --pass dbadminpassword

- Go to http://mwiki.cooponscitech.in/ for your own mediawiki
