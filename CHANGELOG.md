# Changelog

## [0.5.0] - 2022-06-16

### Changed

- Change root dir to allow browsing of mediawiki at root path

## [0.4.0] - 2022-06-16

### Added

- Instructions in README
- Add TODO

## [0.3.0] - 2022-06-16

### Added

- Gitlab CI/CD YAML
- K8s yaml for app & db

## [0.2.0] - 2022-06-16

### Added

- Dockerfile for mediawiki
- LocalSettings.php for mediawiki
- Initial k8s file

## [0.1.0] - 2022-06-11

### Added

- Initial commit
